#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include<sys/ioctl.h>
#include "timer_event.h"

#define BUF_LEN 50

int file_desc;
volatile char done = 0x0;
char arr[BUF_LEN];
char readArr[100];

static sem_t startSemaphore1;
static sem_t startSemaphore2;
static sem_t startSemaphore3;

/*Obezbediti proveru za BUF_LEN > 50*/

void* thread1 (void *pParam)
{
    		
	while(done != 'q')
	{
        	sem_wait(&startSemaphore1);
       		memset(arr, 0, BUF_LEN*sizeof(arr[0]));
        	int k = rand() % 40 + 10;
        	for(int i = 0; i<k; i++) {
        		char ch = 'A' + (rand() % 26);
        		arr[i] = ch;
        	}
        	printf("%s \n", arr);
        	printf("Prva nit kreirala podatke. \n");
    		fflush(stdout);
	
		ioctl(file_desc, 0, arr); //upisi u karakterni drajver kreirane podatke
		
		sem_post(&startSemaphore2);
	}

	return NULL;
}

void* thread2 (void *pParam)
{
	while(done != 'q')
	{
        	sem_wait(&startSemaphore2);
        	
        	ioctl(file_desc, 1, readArr);//procitaj podatke iz karakternog drajvera
        	printf("Procitani dekriptovani podaci: %s: ", readArr);
        	printf("Druga nit primila podatke. \n");
    		fflush(stdout);
	
		ioctl(file_desc, 0, readArr);//upisi podatke u karakterni uredjaj na dekripciju
		
		sem_post(&startSemaphore3);
	}

	return NULL;
}

void* thread3 (void *pParam)
{	
	while(done != 'q')
	{
        	sem_wait(&startSemaphore3);
        	
        	ioctl(file_desc, 1, readArr) //procitaj podatke iz karakternog drajvera
        	printf("Procitani enkriptovani podaci: %s ". readArr);
        	printf("Treca nit. \n");
    		fflush(stdout);
	
	}

	return NULL;
}

void* get_input(void* param)
{
	while (!done)
	{
		scanf("%c", &done);
	}
	return NULL;
}

void clear_screen(void)
{
	const char* CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J";
	write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

void* timer (void *param)
{
	sem_post(&startSemaphore1);
	return 0;
}

int main()
{

	file_desc = open("/dev/secure_mailbox", O_RDWR);
        if(file_desc < 0) {
                printf("Cannot open device file...\n");
                return 0;
        }

	
	pthread_t t1;
	pthread_t t2;
	pthread_t t3;
	pthread_t quit_thread;
	timer_event_t hPrintStateTimer;
	
	sem_init(&startSemaphore1, 0, 0);
	sem_init(&startSemaphore2, 0, 0);
	sem_init(&startSemaphore3, 0, 0);
	
	pthread_create(&t1, NULL, thread1, NULL);
	pthread_create(&t2, NULL, thread2, NULL);
	pthread_create(&t3, NULL, thread3, NULL);
	
	pthread_create(&quit_thread, NULL, get_input, NULL);
	
	timer_event_set(&hPrintStateTimer, 2000, timer, 0, TE_KIND_REPETITIVE);
	
	pthread_join(t3, NULL);
	pthread_join(t2, NULL);
	pthread_join(t1, NULL);
	pthread_join(quit_thread, NULL);
	
	sem_destroy(&startSemaphore1);
	sem_destroy(&startSemaphore2);
	sem_destroy(&startSemaphore3);

    return 0;
}
